"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const userController = require("./User.controller");
const express = require("express");
const app = express.Router();
app.get("/login", userController.getLogin);
app.post("/login", userController.postLogin);
app.get("/logout", userController.logout);
app.get("/forgot", userController.getForgot);
app.post("/forgot", userController.postForgot);
app.get("/reset/:token", userController.getReset);
app.post("/reset/:token", userController.postReset);
app.get("/signup", userController.getSignup);
app.post("/signup", userController.postSignup);
module.exports = "user";
//# sourceMappingURL=user.route.js.map