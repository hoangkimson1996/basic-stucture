"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const apiController = require("./api.controller");
const passportConfig = require("../../config/passport");
const express = require("express");
const app = express();
app.get("/api", apiController.getApi);
app.get("/api/facebook", passportConfig.isAuthenticated, passportConfig.isAuthorized, apiController.getFacebook);
module.exports = "api";
//# sourceMappingURL=api.route.js.map