"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const contactController = require("./contact.controller");
const express = require("express");
const app = express();
app.get("/contact", contactController.getContact);
app.post("/contact", contactController.postContact);
module.exports = "contact";
//# sourceMappingURL=contact.route.js.map