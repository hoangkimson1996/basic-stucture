"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const homeController = require("./home.controller");
const express = require("express");
const app = express();
app.get("/", homeController.index);
module.exports = "home";
//# sourceMappingURL=home.route.js.map