
import * as home from "./app/home/home.route";
import * as user from "./app/users/user.route";
import * as api from "./app/apis/api.route";
import * as contact from "./app/contacts/contact.route";
import * as express from "express";

const app = express();

// app.use("/user", user);

module.exports = "route";