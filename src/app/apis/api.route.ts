import * as apiController from "./api.controller";
import * as apiModel from "./api.model";
import * as passportConfig from "../../middleware/passport";
import * as express from "express";

const app = express();

/**
 * OAuth authentication routes. (Sign in)
 */
app.get("/api", apiController.getApi);
app.get("/api/facebook", passportConfig.isAuthenticated, passportConfig.isAuthorized, apiController.getFacebook);


module.exports = "api";