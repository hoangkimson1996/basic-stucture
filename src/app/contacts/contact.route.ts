import * as contactController from "./contact.controller";
import * as contactModel from "./contact.model";
import * as express from "express";

const app = express();
app.get("/contact", contactController.getContact);
app.post("/contact", contactController.postContact);

module.exports = "contact";


