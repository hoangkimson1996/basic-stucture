import * as homeController from "./home.controller";
import * as homeModel from "./home.model";
import * as express from "express";

const app = express();

app.get("/", homeController.index);

module.exports = "home";
