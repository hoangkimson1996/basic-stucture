import * as mongoose from "mongoose";
import * as session from "express-session";
import * as mongo from "connect-mongo";

const MongoStore = mongo(session);

/**
 * Connect to MongoDB.
 */
// mongoose.Promise = global.Promise;
mongoose.connect(process.env.MONGODB_URI || process.env.MONGOLAB_URI);

mongoose.connection.on("error", () => {
  console.log("MongoDB connection error. Please make sure MongoDB is running.");
  process.exit();
});